import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import React, { useState } from 'react';
import CreateCourse from './components/CourseForm/CourseForm';
import Login from './components/Login/Login';
import Registration from './components/Registartion/Registration';
import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { ProtectedRoute } from './components/Login/protectedRoute';
import { logOut } from './services';
import { filterFunc } from './helpers/authorConstructor';
import { PivateRouter } from './components/PrivateRouter/PrivateRouter';

import { setLogoutCreator } from './store/user/actionCreators';
import { useDispatch, useSelector } from 'react-redux';
import { setLoginCreator } from './store/user/actionCreators';
import { saveNewCourseDB } from './store/courses/thunk';

function App() {
  const [token, setToken] = useState(null);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  console.log(useSelector((state) => state)); /////////////// console

  const coursesData = useSelector((state) => state.courses); // курсы со стора
  const authorsData = useSelector((state) => state.authors);

  const filteredCoursesData = filterFunc(coursesData, authorsData);

  const loginHandler = (dbResponse) => {
    if (dbResponse.result === undefined) {
      alert('You are not registered yet');
    } else if (dbResponse.result === 'Invalid data.') {
      alert('Invalid data');
    } else {
      if (dbResponse.user.email === 'admin@email.com') {
        localStorage.setItem('token', JSON.stringify(dbResponse.result));
        localStorage.setItem('role', 'admin');
      } else {
        localStorage.setItem('token', JSON.stringify(dbResponse.result));
        localStorage.setItem('role', 'user');
      }
      setToken(dbResponse.result);
      dispatch(
        setLoginCreator({
          ...dbResponse,
          role: localStorage.getItem('role'),
        })
      ); /////////// dispatch
      navigate('/courses', { replace: true });
    }
  };

  const logoutHandler = (token) => {
    logOut(token);
    setToken(null);
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    dispatch(setLogoutCreator()); /////////// dispatch
  };

  const onSaveCourseData = (inputCourseData) => {
    const newCourse = {
      ...inputCourseData,
    };
    dispatch(saveNewCourseDB(newCourse, token)); //// dispatch
  };

  return (
    <div>
      <Header
        token={localStorage.getItem('token')}
        onLogout={() => logoutHandler(token)}
      />
      <Routes>
        <Route
          path='/'
          element={
            localStorage.getItem('token') ? (
              <Navigate to='/courses' />
            ) : (
              <Navigate to='/login' />
            )
          }
        />
        <Route path='/login' element={<Login onLogin={loginHandler} />} />
        <Route path='/registration' element={<Registration />} />
        <Route
          path='/courses'
          element={
            <ProtectedRoute>
              <Courses courses={filteredCoursesData} />
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/add'
          element={
            <ProtectedRoute>
              <PivateRouter>
                <CreateCourse
                  onSaveCourse={onSaveCourseData}
                  authors={authorsData}
                />
              </PivateRouter>
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/:courseId'
          element={<CourseInfo courses={filteredCoursesData} />}
        />
        <Route
          path='/courses/update/:courseId'
          element={<CreateCourse courses={filteredCoursesData} />}
        />
        <Route
          path='*'
          element={<p style={{ fontSize: '1.3rem' }}>Page is not found.</p>}
        />
      </Routes>
    </div>
  );
}

export default App;
