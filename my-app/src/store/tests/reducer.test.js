import coursesReducer from '../courses/reducer';
import * as types from '../courses/actionTypes';

describe('coursesReducer', () => {
  it('should return the initial state when passed an empty action', () => {
    const result = coursesReducer(undefined, { type: '' });
    expect(result).toEqual([]);
  });

  it('should handle SAVE_COURSE and returns new state', () => {
    const action = {
      type: types.SAVE_NEW_COURSE,
      payload: {
        title: 'Java',
      },
    };
    const result = coursesReducer([], action);
    expect(result).toEqual([
      {
        title: 'Java',
      },
    ]);
  });

  it('should handle GET_COURSES and returns new state', () => {
    const action = {
      type: types.GET_COURSES,
      payload: {
        result: [
          {
            title: 'Java',
          },
          {
            title: 'JS',
          },
        ],
      },
    };
    const result = coursesReducer([], action);
    expect(result).toEqual([
      {
        title: 'Java',
      },
      {
        title: 'JS',
      },
    ]);
  });
});
