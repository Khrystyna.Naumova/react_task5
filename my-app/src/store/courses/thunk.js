import {
  saveNewCourseCreator,
  deleteCourseCreator,
  updateCourseCreator,
} from './actionCreators';

export const saveNewCourseDB = (newCourse, token) => {
  return (dispatch) => {
    fetch('http://localhost:4000/courses/add', {
      method: 'POST',
      body: JSON.stringify(newCourse),
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then((res) => res.json())
      .then((data) => dispatch(saveNewCourseCreator(data.result)));
  };
};

export const updateCourseDB = (updCourse, token) => {
  return (dispatch) => {
    fetch(`http://localhost:4000/courses/${updCourse.id}`, {
      method: 'PUT',
      body: JSON.stringify(updCourse),
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then((res) => res.json())
      .then((data) => dispatch(updateCourseCreator(data.result)));
  };
};

export const deleteCourseDB = (course, token) => {
  return (dispatch) => {
    fetch(`http://localhost:4000/courses/${course.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    }).then((res) => dispatch(deleteCourseCreator(course)));
  };
};
