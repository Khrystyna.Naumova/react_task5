import * as actions from './actionTypes';

export const saveNewCourseCreator = (payload) => ({
  type: actions.SAVE_NEW_COURSE,
  payload: payload,
});

export const deleteCourseCreator = (payload) => ({
  type: actions.DELETE_COURSE,
  payload: payload.id,
});

export const updateCourseCreator = (payload) => ({
  type: actions.UPDATE_COURSE,
  payload,
});

export const getCoursesCreator = (payload) => ({
  type: actions.GET_COURSES,
  payload,
});
