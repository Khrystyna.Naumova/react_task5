import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { userReducer } from './user/reducer';
import { authorsReducer } from './authors/reducer';
import { coursesReducer } from './courses/reducer';
import thunkMiddleware from 'redux-thunk';
import { applyMiddleware } from 'redux';

const rootReducer = combineReducers({
  authors: authorsReducer,
  courses: coursesReducer,
  user: userReducer,
});

export const store = configureStore({
  reducer: rootReducer,
  async: applyMiddleware(thunkMiddleware),
});
