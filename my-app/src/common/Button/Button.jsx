import './Button.css';

function Button(props) {
  return (
    <button
      className={props.className}
      type={props.type || 'button'}
      style={props.style}
      onClick={props.handleClick}
      disabled={props.disabled}
    >
      {props.buttonText}
    </button>
  );
}

export default Button;
