function pipeDuration(min) {
  const hours = min / 60; //2.66
  const rhours = Math.floor(hours); //2
  const minutes = (hours - rhours) * 60; //0.66 * 60 = 39.6
  const rminutes = Math.round(minutes); //40
  return `${rhours}:${rminutes} hours`; // 2.40
}

export default pipeDuration;
