export function filterFunc(coursesData, authorsData) {
  const filteredData = coursesData.map((item) => {
    const authors = item.authors.map((authorId) => {
      const issetId = authorsData.filter((_author) => authorId === _author.id);
      if (issetId.length) {
        return issetId[0].name;
      }
      return authorId;
    });
    return {
      ...item,
      authors,
    };
  });
  return filteredData;
}

// export function authorArraySort(list) {
//   const authorNames = list.map((author) => {
//     return author.name;
//   });
//   return authorNames;
// }
