import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import CourseCard from '../CourseCard';
import * as reduxHooks from 'react-redux';

jest.mock('react-redux');

// const mockedDispatch = jest.spyOn(reduxHooks, 'useDispatch');

const title = 'Java';
const description = 'Lorem ipsun';
const duration = 120;
const authors = ['Vasiliy Dobkin', 'Nicolas Kim'];
const date = new Date().toLocaleDateString('en-GB');

describe('CourseCard component', () => {
  it('CourseCard should display title', () => {
    // mockedDispatch.mockResolvedValue(jest.fn());
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseCard
          id='123'
          title='Java'
          description='Lorem ipsun'
          authors={['Vasiliy Dobkin', 'Nicolas Kim']}
          duration={120}
          creationDate={'10/11/2020'}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(title)).toBeDefined();
  });

  it('CourseCard should display description', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseCard
          id='123'
          title='Java'
          description='Lorem ipsun'
          authors={['Vasiliy Dobkin', 'Nicolas Kim']}
          duration={120}
          creationDate={'10/11/2020'}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(description)).toBeDefined();
  });

  it('CourseCard should display duration', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseCard
          id='123'
          title='Java'
          description='Lorem ipsun'
          authors={['Vasiliy Dobkin', 'Nicolas Kim']}
          duration={120}
          creationDate={'10/11/2020'}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(duration)).toBeDefined();
  });

  it('CourseCard should display authors list', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseCard
          id='123'
          title='Java'
          description='Lorem ipsun'
          authors={['Vasiliy Dobkin', 'Nicolas Kim']}
          duration={120}
          creationDate={'10/11/2020'}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(authors)).toBeDefined();
  });

  it('CourseCard should display created date', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseCard
          id='123'
          title='Java'
          description='Lorem ipsun'
          authors={['Vasiliy Dobkin', 'Nicolas Kim']}
          duration={120}
          creationDate={'10/11/2020'}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(date)).toBeDefined();
  });
});
