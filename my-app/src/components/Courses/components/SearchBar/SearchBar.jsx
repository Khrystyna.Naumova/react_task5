import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';

import './SearchBar.css';

function SearchBar(props) {
  const searchTitleHandler = (event) => {
    props.onChangeTitle(event.target.value);
  };

  const onSearchHandler = () => {
    props.onSearch();
  };

  return (
    <div className='search'>
      <Input
        value={props.title}
        handleChange={searchTitleHandler}
        placeholderText='Enter course name...'
      />
      <Button
        className='button'
        buttonText='Search'
        handleClick={onSearchHandler}
      />
    </div>
  );
}

export default SearchBar;
