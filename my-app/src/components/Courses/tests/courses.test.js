import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import Courses from '../Courses';
import CourseForm from '../../CourseForm/CourseForm';
import * as reduxHooks from 'react-redux';

jest.mock('react-redux');

const courses = [
  {
    title: 'Java',
  },
  {
    title: 'JS',
  },
  {
    title: 'Angular',
  },
];

const courses2 = [];

describe('Courses component', () => {
  it('Courses should display amount of CourseCard equal length of courses array', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <Courses courses={[]} />
      </Router>
    );
    expect(screen.queryByDisplayValue(courses)).toBeDefined();
  });

  it('Courses should display Empty container if courses array length is 0.', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <Courses courses={[]} />
      </Router>
    );
    expect(courses2.length).toBe(0);
  });

  // it('CourseForm should be showed after a click on a button "Add new course".', () => {
  //   // mockedDispatch.mockResolvedValue(jest.fn());
  //   jest.spyOn(reduxHooks, 'useSelector');
  //   render(
  //     <Router>
  //       <Courses courses={[]} />
  //     </Router>
  //   );
  //   fireEvent.click(screen.getByRole('button', { name: 'Add new course' }));
  //   expect(screen.queryByDisplayValue(<CourseForm />)).toBeDefined();
  // });
});
