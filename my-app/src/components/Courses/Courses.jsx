import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import './Courses.css';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Courses(props) {
  // Use two pieces of state; one to track the value in your text field and the other to store the search term for filtering. Only set the latter when you click your button

  const [searchValue, setSearchValue] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const role = localStorage.getItem('role');

  const filteredCourses = props.courses.filter((course) => {
    return course.title.toLowerCase().includes(searchTerm.toLowerCase());
  });

  const searchTitleHandler = (title) => {
    setSearchValue(title);
  };

  const onSearchButton = () => {
    setSearchTerm(searchValue);
  };

  return (
    <div className='courses'>
      <div className='searchBar'>
        <SearchBar
          title={searchValue}
          onChangeTitle={searchTitleHandler}
          onSearch={onSearchButton}
        />
        {role === 'admin' && (
          <Link to='/courses/add'>
            <Button className='button' buttonText='Add new course' />
          </Link>
        )}
      </div>
      {filteredCourses.length === 0 ? (
        <p
          style={{
            marginLeft: '1.4rem',
            color: 'darkred',
            fontSize: '1.2rem',
          }}
        >
          No matches courses.
        </p>
      ) : (
        filteredCourses.map((course) => (
          <CourseCard
            key={course.id}
            id={course.id}
            title={course.title}
            description={course.description}
            authors={course.authors}
            duration={course.duration}
            creationDate={course.creationDate}
          />
        ))
      )}
    </div>
  );
}

export default Courses;
