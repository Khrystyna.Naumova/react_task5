import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from '../Header.jsx';
// import { useSelector } from 'react-redux'; // 1 вариант
import * as reduxHooks from 'react-redux'; // 2 вариант

//Header should have logo and user's name.
jest.mock('react-redux');

const userName = 'Jack';

describe('Header component', () => {
  it('Header renders <Logo />', () => {
    // useSelector.mockReturnValue([]);// 1 вариант
    jest.spyOn(reduxHooks, 'useSelector').mockReturnValue([]); //джест шпионь плз за .. // 2 вариант
    render(<Header />);
    const logo = screen.queryByTestId('logo');
    expect(logo).toBeDefined();
  });

  it('Header renders user name', () => {
    // useSelector.mockReturnValue(userName);// 1 вариант
    jest.spyOn(reduxHooks, 'useSelector').mockReturnValue(userName); //джест шпионь плз за .. // 2 вариант
    render(<Header />);
    expect(screen.queryByDisplayValue(userName)).toBeDefined();
  });
});
