import React, { useEffect, useState } from 'react';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import styles from './Login.module.css';
import { Link } from 'react-router-dom';

const Login = (props) => {
  const [inputEmail, setInputEmail] = useState('');
  const [inputPassword, setInputPassword] = useState('');
  const [emailIsValid, setEmailIsValid] = useState();
  const [passwordIsValid, setPasswordIsValid] = useState();

  const [formIsValid, setFormIsValid] = useState(false);

  useEffect(() => {
    // чтоб не было переоценки ком-нта
    const timer = setTimeout(() => {
      setFormIsValid(
        inputEmail.includes('@') && inputPassword.trim().length > 5
      );
    }, 800); //если пауза 0.8 сек то проверяем на валидность
    return () => {
      clearTimeout(timer);
    };
  }, [inputEmail, inputPassword]);

  const emailChangeHandler = (event) => {
    setInputEmail(event.target.value);
  };

  const passwordChangeHandler = (event) => {
    setInputPassword(event.target.value);
  };

  const validateEmailHandler = () => {
    setEmailIsValid(inputEmail.includes('@'));
  };

  const validatePasswordHandler = () => {
    setPasswordIsValid(inputPassword.trim().length > 5);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const user = {
      email: inputEmail,
      password: inputPassword,
    };
    loginUser(user);
  };

  const loginUser = (user) => {
    fetch('http://localhost:4000/login', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((response) => response.json())
      .then((dbResponse) => {
        props.onLogin(dbResponse);
      });
  };

  return (
    <div className={styles.login}>
      <h1>Login</h1>
      <form onSubmit={submitHandler} className={styles.loginForm}>
        <div className={`${emailIsValid === false ? styles.invalid : ''}`}>
          <label htmlFor='email'>Email</label>
          <Input
            type='email'
            id='email'
            placeholderText='Enter Email'
            value={inputEmail}
            handleChange={emailChangeHandler}
            onBlur={validateEmailHandler}
          />
        </div>
        <div className={` ${passwordIsValid === false ? styles.invalid : ''}`}>
          <label htmlFor='password'>Password</label>
          <Input
            type='password'
            id='password'
            placeholderText='Enter password'
            value={inputPassword}
            handleChange={passwordChangeHandler}
            onBlur={validatePasswordHandler}
          />
        </div>
        <div className={styles.actions}>
          <Button
            className='button'
            type='submit'
            disabled={!formIsValid}
            buttonText='Login'
          />
        </div>
        <p>
          {'If you not have an account you can '}
          <Link to='/registration'>Registration</Link>
        </p>
      </form>
    </div>
  );
};

export default Login;
