import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import CourseForm from '../CourseForm';
import * as reduxHooks from 'react-redux';
import * as actions from '../../../store/authors/actionTypes';

jest.mock('react-redux');

const authors = [
  { id: '123', name: 'Vasya' },
  { id: '345', name: 'Petya' },
];

const mockedDispatch = jest.spyOn(reduxHooks, 'useDispatch');

describe('CourseForm component', () => {
  it('CourseForm should show authors lists', () => {
    jest.spyOn(reduxHooks, 'useSelector');
    render(
      <Router>
        <CourseForm
          authors={[
            { id: '123', name: 'Vasya' },
            { id: '345', name: 'Petya' },
          ]}
          courses={[]}
          onSaveCourse={jest.fn()}
        />
      </Router>
    );
    expect(screen.queryByDisplayValue(authors)).toBeDefined();
  });

  it('CourseForm "Create author" click button should call dispatch.', () => {
    render(
      <Router>
        <CourseForm
          authors={[
            { id: '123', name: 'Vasya' },
            { id: '345', name: 'Petya' },
          ]}
          courses={[]}
          onSaveCourse={jest.fn()}
        />
      </Router>
    );
    fireEvent.click(screen.getByRole('button', { name: 'Create author' }));
    expect(mockedDispatch).toHaveBeenCalledTimes(1);
  });

  // it('CourseForm "Add author" button click should add an author to course authors list.', () => {
  //   render(
  //     <Router>
  //       <CourseForm
  //         authors={[
  //           { id: '123', name: 'Vasya' },
  //           { id: '345', name: 'Petya' },
  //         ]}
  //         courses={[]}
  //         onSaveCourse={jest.fn()}
  //       />
  //     </Router>
  //   );
  //   fireEvent.click(screen.getByRole('button', { name: 'Create author' }));
  //   expect().toEqual([]);
  // });
});
