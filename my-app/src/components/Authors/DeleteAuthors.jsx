import Button from '../../common/Button/Button';

function DeleteAuthors(props) {
  return (
    <div className='createCourse__authors'>
      <p>{props.name}</p>
      <Button
        className='button'
        type='button'
        buttonText='Delete author'
        handleClick={props.deleteAuthor}
      />
    </div>
  );
}

export default DeleteAuthors;
